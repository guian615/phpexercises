<?php 
    include "link.php"
?>
<body>
    <div class="container text-center mt-5" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
        <h1>
            Exercise 1
        </h1>
        <h2>
            Write a program to loop over the given JSON data. Display the values via loops or recursion.
        </h2>
    </div> 
        <div class="mt-5 container"  style="margin-left:580px;">
            <?php
                $json_data = '[
                    {
                    "name" : "John Garg",
                    "age"  : "15",
                    "school" : "Ahlcon Public school"
                    },
                    {
                    "name" : "Smith Soy",
                    "age"  : "16",
                    "school" : "St. Marie school"
                    },
                    {
                    "name" : "Charle Rena",
                    "age"  : "16",
                    "school" : "St. Columba school"
                    }
                ]';
                $someArrays = json_decode($json_data, true);
                foreach ($someArrays as $key => $value) {
                    echo '<span style="color:blue;font-weight:bold;font-size:20px;">Name: </span>'. $value["name"] .", <br>".'<span style="color:blue;font-weight:bold;font-size:20px;">Age: </span>'.$value["age"] . ", <br>" .'<span style="color:blue;font-weight:bold;font-size:20px;">School: </span>' .$value["school"] . "<br>";
                }
            ?> 
        </div>
</body>
