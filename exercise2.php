<?php 
    include "link.php"
?>
<body>
    <div class="container mt-5 text-center">
     <div style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
        <h1>
            Exercise 2
        </h1>
        <div class="mt-3">
        <h3>
            There are two deals of an item to buy. The quantities and prices of the item are given below. Write a program in PHP to find the best deal to purchase the item.
        </h3> 
        </div>
     </div>
    <?php 
        $quantity1 = 70;
        $price1 = 35;
        $price2 = 30;
        $quantity2 = 100;

        $deal1 = $price1/$quantity1;
        $deal2 = $price2/$quantity2;

        echo ("<h2>Deal 1 about :</h2>");
        echo "<h4>Price: $price1</h4>";
        echo "<h4>Quantity: $quantity1</h4>";
        echo"<br>";
        echo ("<h2>Deal 2 about :</h2>");
        echo "<h4>Price: $price2</h4>";
        echo "<h4>Quantity: $quantity2</h4>";

        if($deal1<$deal2){
            echo "Deal 1 is the best!";
        }else{

            echo "<h1 style ='color:blue'>Deal 2 is the best!</h1>";
        }
    ?>