<?php 
    include "link.php"
?>

<body>
<div class="container mt-5 text-center" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
    <h2>Exercise 3</h2>
    <h2>Division table</h2>
        <div class="mb-4">
            <h3>Write a division table program using the for loop.</h3>
        </div>
</div>
    <center>
        <TABLE BORDER=2 style="background-color:skyblue;">
        <?php
        $start_num = 1;
        $end_num = 10;
        echo("<tr>");
        echo("<th> </th>");
        for ($count_1 = $start_num;$count_1 <= $end_num;$count_1++)
            echo("<th>$count_1</th>");
        echo("</tr>");
        for ($count_1 = $start_num;$count_1 <= $end_num;$count_1++){
            echo("<tr><th>$count_1</th>");
            for ($count_2 = $start_num;$count_2 <= $end_num;$count_2++){
                $result =$count_2 / $count_1;
                printf("<td>%.2f</td>",
                    $result); 
            }
            echo("</tr>\n");
        }
        ?> 
    </center>
</body>