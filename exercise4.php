<?php 
    include "link.php"
?>

<body>
    <div class="container mt-5 text-center">
     <div style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
        <h1>
            Exercise 4
        </h1>
        <div class="mt-3">
        <h3>
            Write a program to determine if the number is an Armstrong Number. An Armstrong Number is a number such that the sum of the cubes of its digits is equal to the number itself.
        </h3> 
        </div>
     </div>
        <form action="exercise4.php" method="post">
        <div class="mt-4">
            <input class="form-control" id="input" value="" name="number1" type="number" placeholder="Input a Number">
        </div>
        <div class="mt-4">
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
        <?php
        if(isset($_POST['submit'])){
                $num=$_POST['number1'];
                $sum=0;
                $temp=$num;
            while($temp!=0)
            {
                $rem=$temp%10;
                $sum=$sum+$rem*$rem*$rem;
                $temp=$temp/10;
            }
            if($num==$sum ){
                echo "<script>alert('Armstrong number')</script>";
                
            }else
            {
                echo "<script>alert('Not Armstrong number')</script>";
                
            }
        }
        ?>
    </div>
</body>