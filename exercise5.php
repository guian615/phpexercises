<?php 
    include "link.php"
?>

<body>
    <div class="container mt-5 text-center">
        <div style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
            <h1>
                Exercise 5
            </h1>
            <div class="mt-3">
                <h3>
                Write a program to delete the recurring elements inside a sorted list of integers. 
                </h3> 
        </div>
    </div>
            <div class="mt-5">
            <?php
                $arr = array(1, 2, 2, 3, 1, 9, 2, 4, 5,3,1,0);
                sort($arr);
                echo '<span style="color:blue;font-weight:bold">Original Array:</span>';            
                echo"<pre>";
                print_r($arr);
                echo '<span style="color:red;font-weight:bold">Updated Array:</span>';            
                echo"<pre>";
                print_r(array_unique($arr));
            ?>
         </div>
    </div>
</body>