<?php 
    include "link.php"
?>

<body>
    <div class="container mt-5 text-center" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
        <h1>
            Exercise 6
        </h1>
        <div class="mt-3">
            <h3>
                Write a program to delete the recurring elements inside a sorted list of strings  
          </h3> 
        </div>
    </div>
<div class="container mt-5" style="margin-left:580px;">
        <?php
            $arr = array("Marjorie","Patrick","Patrick","Justine","Guian","Dave","Rubylyn","Jessa","Guian","Dave","Miguel","Melchor");
            sort($arr);
            echo '<span style="color:blue;font-weight:bold">Original Array:</span>';            
            echo"<pre>";
            print_r($arr);
            echo '<span style="color:red;font-weight:bold">Updated Array:</span>';            
            echo"<pre>";
            print_r(array_unique($arr));

        ?>
        </div>
</body>